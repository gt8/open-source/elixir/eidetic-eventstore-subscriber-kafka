defmodule Eidetic.EventStore.Subscriber.Kafka do
  @moduledoc """
  Documentation for Kafka.
  """

  use GenServer
  require Logger

  @doc false
  def start_link(options \\ []) do
    Logger.info fn() -> "Starting Kaffe Producer" end
    Kaffe.Producer.start_producer_client()

    GenServer.start_link(__MODULE__, %{
      helper: Application.get_env(:eidetic_eventstore_subscriber_kafka, :helper)
    }, name: __MODULE__)
  end

  @doc false
  def handle_cast({:publish, event = %Eidetic.Event{}}, state) do
    case Kaffe.Producer.produce_sync(
      state[:helper].topic(event),
      state[:helper].partition(event),
      "Some Key",
      Poison.encode!(event)
    ) do
      :ok ->
        Logger.debug("Published succesfully to Kafka")
        :ok
      error ->
        Logger.error("Failed to publish to Kafka: #{inspect error}")
        :error
    end

    {:noreply, state}
  end
end
